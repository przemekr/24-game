//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#ifndef CARD_H
#define CARD_H
#include "agg_gsv_text.h"
#include "agg_rounded_rect.h"
#include "ctrl/agg_ctrl.h"

enum Suite
{
   HEART = 0,
   DAIMOND,
   SPADE,
   CLUBS
};

enum OperType
{
   EMPTY = 0,
   ADD,
   SUB,
   MLT,
   DIV
};

double calc(OperType oper, double x, double y)
{
   switch(oper)
   {
      case EMPTY:
         return 0;
      case ADD:
         return x + y;
      case SUB:
         return x - y;
      case MLT:
         return x * y;
      case DIV:
         return x / y;
   }
}
Suite suiteFromInt(int x) { return (Suite)(x%4); }


template<class ColorT> class Card : public agg::ctrl
{
public:
   Card(double x1, double y1, double x2, double y2,
         int v, Suite s) :
      ctrl(x1, y1, x2, y2, false),
      m_text_thickness(1.5),
      m_text_height(0.4*(y2-y1)),
      m_text_width(0.8*(x2-x1)/2),
      m_status(false),
      m_clicked(false),
      outline(x1, y1, x2, y2, 5.0),
      shadow(x1+2*m_text_thickness, y1-m_text_thickness, x2+2*m_text_thickness, y2-m_text_thickness, 5.0),
      background(x1+m_text_thickness, y1+m_text_thickness, x2-m_text_thickness, y2-m_text_thickness, 5.0),
      m_text_poly(m_text),
      suite(s)
   {
      value(v);
   }

   //------------------------------------------------------------------------
   void text_size(double h, double w)
   {
      m_text_width = w; 
      m_text_height = h; 
   }

   //------------------------------------------------------------------------
   int  value() { return val; }
   int  value(int v)
   {
      val = v;
      sprintf(m_label, "%d", val);
      return val;
   }

   //------------------------------------------------------------------------
   bool on_mouse_button_down(double x, double y)
   {
      inverse_transform_xy(&x, &y);
      if(x >= m_x1 && y >= m_y1 && x <= m_x2 && y <= m_y2)
      {
         m_clicked = true;
         return true;
      }
      return false;
   }

   //------------------------------------------------------------------------
   bool on_mouse_move(double, double, bool)
   {
      return false;
   }

   //------------------------------------------------------------------------
   bool in_rect(double x, double y) const
   {
      inverse_transform_xy(&x, &y);
      return x >= m_x1 && y >= m_y1 && x <= m_x2 && y <= m_y2;
   }

   //------------------------------------------------------------------------
   bool on_mouse_button_up(double x, double y)
   {
      inverse_transform_xy(&x, &y);
      m_clicked = false;
      if(x >= m_x1 && y >= m_y1 && x <= m_x2 && y <= m_y2)
      {
         m_status = !m_status;
         return true;
      }
      return false;
   }

   //------------------------------------------------------------------------
   bool on_arrow_keys(bool, bool, bool, bool)
   {
      return false;
   }


   //------------------------------------------------------------------------
   void rewind(unsigned idx)
   {
      m_idx = idx;

      double d2;
      double t;

      switch(idx)
      {
         default:
         case 0:                 // Background
            m_vertex = 0;
            shadow.rewind(0);
            break;

         case 1:                 // Background
            m_vertex = 0;
            outline.rewind(0);
            break;

         case 2:                 // Border
            m_vertex = 0;
            background.rewind(0);
            break;

         case 3:                 // Text
            m_text.text(m_label);
            m_text.start_point(m_x1 + 2*m_text_thickness, m_y1 + 3*m_text_thickness);
            m_text.size(m_text_height, m_text_width);
            m_text_poly.width(m_text_thickness);
            m_text_poly.line_join(agg::round_join);
            m_text_poly.line_cap(agg::round_cap);
            m_text_poly.rewind(0);
            break;
      }
   }

   //------------------------------------------------------------------------
   unsigned vertex(double* x, double* y)
   {
      unsigned cmd = agg::path_cmd_line_to;
      switch(m_idx)
      {
         case 0:
            if (m_clicked)
            {
               cmd = agg::path_cmd_stop;
               break;
            }
            cmd = shadow.vertex(x, y);
            break;

         case 1:
            cmd = outline.vertex(x, y);
            break;
            break;

         case 2:
            cmd = background.vertex(x, y);
            break;

         case 3:
            cmd = m_text_poly.vertex(x, y);
            break;

         default:
            cmd = agg::path_cmd_stop;
            break;
      }

      if(!agg::is_stop(cmd))
      {
         transform_xy(x, y);
      }
      return cmd;
   }

private:
   double   m_text_thickness;
   double   m_text_height;
   double   m_text_width;
   char     m_label[128];
   bool     m_status;
   bool     m_clicked;
   double   m_vx[32];
   double   m_vy[32];
   agg::rounded_rect shadow;
   agg::rounded_rect outline;
   agg::rounded_rect background;
   agg::gsv_text     m_text;
   agg::conv_stroke<agg::gsv_text> m_text_poly;
   unsigned m_idx;
   unsigned m_vertex;
   ColorT text_color;
   ColorT outline_color;
   ColorT background_color;
   ColorT shadow_color;
   ColorT* colors[4];

   int val;
   Suite suite;
};

#endif
