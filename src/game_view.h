#include "ctrl/agg_bezier_ctrl.h"

template<class Pattern, 
   class Rasterizer, 
   class Renderer, 
   class PatternSource, 
class VertexSource>
void draw_curve(Pattern& patt, 
      Rasterizer& ras, 
      Renderer& ren, 
      PatternSource& src, 
      VertexSource& vs)
{
   patt.create(src);
   ren.scale_x(1.0);
   ren.start_x(1.0);
   ras.add_path(vs);
}

static agg::int8u brightness_to_alpha[256 * 3] = 
{
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 254, 254, 254, 254, 254, 254, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 
    254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 253, 253, 
    253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 253, 252, 
    252, 252, 252, 252, 252, 252, 252, 252, 252, 252, 252, 251, 251, 251, 251, 251, 
    251, 251, 251, 251, 250, 250, 250, 250, 250, 250, 250, 250, 249, 249, 249, 249, 
    249, 249, 249, 248, 248, 248, 248, 248, 248, 248, 247, 247, 247, 247, 247, 246, 
    246, 246, 246, 246, 246, 245, 245, 245, 245, 245, 244, 244, 244, 244, 243, 243, 
    243, 243, 243, 242, 242, 242, 242, 241, 241, 241, 241, 240, 240, 240, 239, 239, 
    239, 239, 238, 238, 238, 238, 237, 237, 237, 236, 236, 236, 235, 235, 235, 234, 
    234, 234, 233, 233, 233, 232, 232, 232, 231, 231, 230, 230, 230, 229, 229, 229, 
    228, 228, 227, 227, 227, 226, 226, 225, 225, 224, 224, 224, 223, 223, 222, 222, 
    221, 221, 220, 220, 219, 219, 219, 218, 218, 217, 217, 216, 216, 215, 214, 214, 
    213, 213, 212, 212, 211, 211, 210, 210, 209, 209, 208, 207, 207, 206, 206, 205, 
    204, 204, 203, 203, 202, 201, 201, 200, 200, 199, 198, 198, 197, 196, 196, 195, 
    194, 194, 193, 192, 192, 191, 190, 190, 189, 188, 188, 187, 186, 186, 185, 184, 
    183, 183, 182, 181, 180, 180, 179, 178, 177, 177, 176, 175, 174, 174, 173, 172, 
    171, 171, 170, 169, 168, 167, 166, 166, 165, 164, 163, 162, 162, 161, 160, 159, 
    158, 157, 156, 156, 155, 154, 153, 152, 151, 150, 149, 148, 148, 147, 146, 145, 
    144, 143, 142, 141, 140, 139, 138, 137, 136, 135, 134, 133, 132, 131, 130, 129, 
    128, 128, 127, 125, 124, 123, 122, 121, 120, 119, 118, 117, 116, 115, 114, 113, 
    112, 111, 110, 109, 108, 107, 106, 105, 104, 102, 101, 100,  99,  98,  97,  96,  
     95,  94,  93,  91,  90,  89,  88,  87,  86,  85,  84,  82,  81,  80,  79,  78, 
     77,  75,  74,  73,  72,  71,  70,  69,  67,  66,  65,  64,  63,  61,  60,  59, 
     58,  57,  56,  54,  53,  52,  51,  50,  48,  47,  46,  45,  44,  42,  41,  40, 
     39,  37,  36,  35,  34,  33,  31,  30,  29,  28,  27,  25,  24,  23,  22,  20, 
     19,  18,  17,  15,  14,  13,  12,  11,   9,   8,   7,   6,   4,   3,   2,   1
};


class pattern_src_brightness_to_alpha_rgba8
{
public:
    pattern_src_brightness_to_alpha_rgba8(agg::rendering_buffer& rb) : 
        m_rb(&rb), m_pf(*m_rb) {}

    unsigned width()  const { return m_pf.width();  }
    unsigned height() const { return m_pf.height(); }
    agg::rgba8 pixel(int x, int y) const
    {
        agg::rgba8 c = m_pf.pixel(x, y);
        c.a = brightness_to_alpha[c.r + c.g + c.b];
        return c;
    }

private:
    agg::rendering_buffer* m_rb;
    pixfmt_type m_pf;
};

class GameView : public View
{
public:
   GameView(App& application): app(application),
      menu(80,  20, 180, 40,   "Menu", !flip_y),
      next(220, 20, 320, 40,   "Go  ", !flip_y),
      bracket(340, 20, 370, 40,   "()", !flip_y),

      card1(40,  80,  100, 200,  "PL", !flip_y),
      oper1(110, 120, 140, 160,  " ", !flip_y),
      card2(150, 80,  210, 200,  "AY", !flip_y),
      oper2(220, 120, 250, 160,  " ", !flip_y),
      card3(260, 80,  320, 200,  "2 ", !flip_y),
      oper3(330, 120, 360, 160,  " ", !flip_y),
      card4(370, 80,  430, 200,  "4 ", !flip_y),
      animation(0), bracketPos(0), selCard(0), MAX_FPS(14),
      got_it_right(false)
   {
      menu.background_color(red);
      next.background_color(red);
      bracket.background_color(red);
      card1.background_color(lblue);
      card2.background_color(lblue);
      card3.background_color(lblue);
      card4.background_color(lblue);
      card1.text_thickness(4);
      card2.text_thickness(4);
      card3.text_thickness(4);
      card4.text_thickness(4);
      oper1.background_color(lgreen);
      oper2.background_color(lgreen);
      oper3.background_color(lgreen);
      add_ctrl(next);
      add_ctrl(menu);
      add_ctrl(card1);
      add_ctrl(card2);
      add_ctrl(card3);
      add_ctrl(card4);
      add_ctrl(oper1);
      add_ctrl(oper2);
      add_ctrl(oper3);
      add_ctrl(bracket);

      m_curve1.curve(170, 424, 13, 87, 488, 423, 26, 333);
      add_ctrl(m_curve1);
      m_curve1.no_transform();
      
      app.wait_mode(true);
   }

   const char * nextOper(const char * curr)
   {
      if (strcmp(curr, "+") == 0)
         return "-";
      if (strcmp(curr, "-") == 0)
         return "*";
      if (strcmp(curr, "*") == 0)
         return "/";
      return "+";
   }

   double calc(double x, const char * curr, double y)
   {
      if (strcmp(curr, "") == 0)
         return 0;
      if (strcmp(curr, "+") == 0)
         return x+y;
      if (strcmp(curr, "-") == 0)
         return x-y;
      if (strcmp(curr, "*") == 0)
         return x*y;
      if (strcmp(curr, "/") == 0)
         return x/y;

      return 0;
   }

   virtual void on_ctrl_change()
   {
      if (menu.status())
      {
         menu.status(false);
         app.changeView("menu");
      }

      if (bracket.status())
      {
         bracket.status(false);
         ++bracketPos %= 3;
      }

      if (next.status())
      {
         app.gameStarted();
         char buf[20];
         next.status(false);
         next.label("Next");
         got_it_right = false;
         bool possible;

         if (getResult(24, possible, NULL))
            app.scores += 3;
         else if (possible)
            app.scores--;

         if (app.scores >= 10)
         {
            app.gameFinished();
         }
         
         oper1.label("+");
         oper2.label("+");
         oper3.label("+");

         app.v1 = rand()%12+1;
         app.v2 = rand()%12+1;
         app.v3 = rand()%12+1;
         app.v4 = rand()%12+1;

         sprintf(buf, "%d", app.v1);
         card1.label(buf);
         sprintf(buf, "%d", app.v2);
         card2.label(buf);
         sprintf(buf, "%d", app.v3);
         card3.label(buf);
         sprintf(buf, "%d", app.v4);
         card4.label(buf);

      }

      if (oper1.status())
      {
         oper1.status(false);
         oper1.label(nextOper(oper1.label()));
      }
      if (oper2.status())
      {
         oper2.status(false);
         oper2.label(nextOper(oper2.label()));
      }
      if (oper3.status())
      {
         oper3.status(false);
         oper3.label(nextOper(oper3.label()));
      }

      app.force_redraw();
   }

   virtual void on_resize(int, int)
   {
      app.force_redraw();
      double w = app.rbuf_window().width();
      double h = app.rbuf_window().height();
      
      size = int(std::min(w*0.95, h*0.9));
      hshift = h - size;
      hshift -= hshift/4;
      wshift = w - size;
      wshift /= 2;
   }

   virtual void on_idle()
   {
      int loop_time = app.elapsed_time();
      if (loop_time < 1000/MAX_FPS)
      {
         usleep(1000*(1000/MAX_FPS-loop_time));
      }
      app.start_timer();
      update();
      app.force_redraw();
   }

   void switchCards(int f, int s)
   {
      if (f < 1 || f > 4 || s < 1 || s > 4)
         return;

      if (app.sound)
      {
         app.play_sound(3, 700);
      }

      agg::button_ctrl<agg::rgba8>& fc =
         f == 1? card1:
         f == 2? card2:
         f == 3? card3:
         card4;
      agg::button_ctrl<agg::rgba8>& sc =
         s == 1? card1:
         s == 2? card2:
         s == 3? card3:
         card4;

      int& fv = 
         f == 1? app.v1:
         f == 2? app.v2:
         f == 3? app.v3:
         app.v4;
      int& sv = 
         s == 1? app.v1:
         s == 2? app.v2:
         s == 3? app.v3:
         app.v4;

      int t = sv;
      sv = fv;
      fv = t;

      char tmp[50];
      strcpy(tmp, fc.label());
      fc.label(sc.label());
      sc.label(tmp);
   }

   virtual void on_mouse_button_up(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_up(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
      }
      if (card1.in_rect(x, y))
         switchCards(selCard, 1);
      if (card2.in_rect(x, y))
         switchCards(selCard, 2);
      if (card3.in_rect(x, y))
         switchCards(selCard, 3);
      if (card4.in_rect(x, y))
         switchCards(selCard, 4);
      selCard = 0;
   }

   virtual void on_mouse_button_down(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_down(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
      }
      selCard = 0;
      x0 = x;
      y0 = y;
      lineNr = rand()%9;
      m1x    = x+rand()%400-200;
      m1y    = y+rand()%400-200;
      m2x    = x+rand()%400-200;
      m2y    = y+rand()%400-200;
      

      if (card1.in_rect(x, y))
         selCard = 1;
      if (card2.in_rect(x, y))
         selCard = 2;
      if (card3.in_rect(x, y))
         selCard = 3;
      if (card4.in_rect(x, y))
         selCard = 4;
      m_curve1.curve(x0, y0, m1x, m1y, m2x, m2y, x, y);
   }

   virtual void on_mouse_move(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_move(x, y, (flags & agg::mouse_left) != 0))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
      if (selCard)
      {
         m_curve1.curve(x0, y0, m1x, m1y, m2x, m2y, x, y);
         app.force_redraw();
      }
   }

   virtual void on_multi_gesture(float x, float y,
         float dTheta, float dDist, int numFingers)
   {
   }

   virtual void on_draw()
   {
      pixfmt_type pf(app.rbuf_window());;
      agg::renderer_base<pixfmt_type> rbase(pf);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();
      rbase.clear(black);

      int w = app.rbuf_window().width();
      int h = app.rbuf_window().height();
      double scale = app.rbuf_window().width()/500.0;
      static agg::trans_affine shape_mtx; shape_mtx.reset();
      shape_mtx *= agg::trans_affine_scaling(scale);
      shape_mtx *= agg::trans_affine_translation(0, 0);
      shape_mtx.invert();

      typedef agg::span_interpolator_linear<agg::trans_affine> interpolator_type;
      interpolator_type interpolator(shape_mtx);
      typedef agg::image_accessor_clone<pixfmt_type> img_accessor_type;
      pixfmt_type pixf_img(app.rbuf_img(11));
      img_accessor_type ia(pixf_img);
      typedef agg::span_image_filter_rgba_nn<img_accessor_type,
              interpolator_type> span_gen_type;
      span_gen_type sg(ia, interpolator);
      agg::span_allocator<color_type> sa;
      ras.move_to_d(0,0);
      ras.line_to_d(w,0);
      ras.line_to_d(w,h);
      ras.line_to_d(0,h);
      agg::render_scanlines_aa(ras, sl, rbase, sa, sg);

      //rbase.copy_from(app.rbuf_img(11), 0, 0, 0);

      shape_mtx.invert();
      next.transform(shape_mtx);
      menu.transform(shape_mtx);
      oper1.transform(shape_mtx);
      oper2.transform(shape_mtx);
      oper3.transform(shape_mtx);
      card1.transform(shape_mtx);
      card2.transform(shape_mtx);
      card3.transform(shape_mtx);
      card4.transform(shape_mtx);
      bracket.transform(shape_mtx);

      agg::render_ctrl(ras, sl, rbase, next);
      agg::render_ctrl(ras, sl, rbase, menu);
      agg::render_ctrl(ras, sl, rbase, oper1);
      agg::render_ctrl(ras, sl, rbase, oper2);
      agg::render_ctrl(ras, sl, rbase, oper3);
      agg::render_ctrl(ras, sl, rbase, card1);
      agg::render_ctrl(ras, sl, rbase, card2);
      agg::render_ctrl(ras, sl, rbase, card3);
      agg::render_ctrl(ras, sl, rbase, card4);
      agg::render_ctrl(ras, sl, rbase, bracket);

      static char* text;
      char buf[20];
      bool possible;
      if (!got_it_right && getResult(24, possible, &text))
      {
         got_it_right = true;
         i = 0;
         if (app.sound)
         {
            app.play_sound(rand()%2+1, 700);
         }
      };

      sprintf(buf, "%d", app.scores);

      if (got_it_right)
      {
         agg::rgba blue(0.7, 0, 0.1, 0.4);
         app.draw_text(292*scale, 212*scale, 30*scale+0.3*i,
              blue, 1.3, "24");
         app.wait_mode(false);
         app.draw_text(410*scale,     20*scale, 30*scale, buf);
         agg::rgba yellow(0.7, 0.7, 0.7, std::max(1-(i/20.0), 0.0));
         app.draw_text(430*scale,     30*scale, 14*scale,
               yellow, 1.0, "+3");
      }
      app.draw_text(40, 220*scale, 15*scale, text);
      app.draw_text(410*scale,     20*scale, 30*scale, buf);


      if (selCard)
      {
         pattern_src_brightness_to_alpha_rgba8 p1(app.rbuf_img(lineNr));
         agg::pattern_filter_bilinear_rgba8 fltr;           // Filtering functor


         //-- Create uninitialized and set the source
         pattern_type patt(fltr);        
         renderer_type ren_img(rbase, patt);
         rasterizer_type ras_img(ren_img);

         draw_curve(patt, ras_img, ren_img, p1, m_curve1.curve());
      }

   }

private:
   bool getResult(int expected, bool& possible, char **output)
   {
      static char buf[128];
      const char* fmt;
      int result;

      if (bracketPos == 0)
      {
         result = (int)calc(calc(calc(app.v1, oper1.label(), app.v2), oper2.label(), app.v3), oper3.label(), app.v4); 
         fmt = "((%d %s %d) %s %d) %s %d = %d";
      }
      if (bracketPos == 1)
      {
         result = (int)calc(calc(app.v1, oper1.label(), app.v2), oper2.label(), calc(app.v3, oper3.label(), app.v4));
         fmt = "(%d %s %d) %s (%d %s %d) = %d";
      }
      if (bracketPos == 2)
      {
         result = (int)calc(calc(app.v1, oper1.label(), calc(app.v2, oper2.label(), app.v3)), oper3.label(), app.v4);
         fmt = "%d %s (%d %s %d) %s %d = %d";
      }
      char solution[128];
      possible = solver::isSolution(expected, app.v1, app.v2, app.v3, app.v4, solution);
      sprintf(buf, fmt, app.v1, oper1.label(), app.v2, oper2.label(), app.v3, oper3.label(), app.v4, result); 
      if (output)
         *output = buf;
      return result == expected || result == -expected;
   }

    App& app;
    int size, wshift, hshift;
    agg::button_ctrl<agg::rgba8> next;
    agg::button_ctrl<agg::rgba8> menu;

    agg::button_ctrl<agg::rgba8> oper1;
    agg::button_ctrl<agg::rgba8> oper2;
    agg::button_ctrl<agg::rgba8> oper3;

    agg::button_ctrl<agg::rgba8> card1;
    agg::button_ctrl<agg::rgba8> card2;
    agg::button_ctrl<agg::rgba8> card3;
    agg::button_ctrl<agg::rgba8> card4;
    agg::button_ctrl<agg::rgba8> bracket;

    agg::bezier_ctrl<agg::rgba8> m_curve1;

    int animation;
    int selCard;
    int x0, y0, lineNr, m1x, m1y, m2x, m2y;
    int bracketPos;
    bool got_it_right;
    int i;
    const int MAX_FPS;

    void update()
    {
       i++;
       if (got_it_right && i > 1.5*MAX_FPS)
       {
          got_it_right = false;
          next.status(true);
          on_ctrl_change();
       }
    }
};

